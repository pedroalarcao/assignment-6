import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class P9_5Test {

        @Test
        void occursOn() {
            Appointment[] appointments = new Appointment[3];
            appointments[0] = new Daily("Walk dog", 2020, 10, 15);
            appointments[1] = new Monthly("Pay rent", 2020, 10, 15);
            appointments[2] = new Onetime("Complete essay", 2020, 10, 15);


            assertTrue(appointments[0].occursOn(2020, 10, 15));
            assertTrue(appointments[1].occursOn(2020, 10, 15));
            assertTrue(appointments[2].occursOn(2020, 10, 15));

            assertTrue(appointments[0].occursOn(2020, 11, 15));
            assertTrue(appointments[1].occursOn(2020, 11, 15));
            assertFalse(appointments[2].occursOn(2020, 11, 15));

            assertTrue(appointments[0].occursOn(2020, 12, 5));
            assertFalse(appointments[1].occursOn(2020, 12, 5));
            assertFalse(appointments[2].occursOn(2020, 12, 5));

            assertFalse(appointments[0].occursOn(2020, 9, 15));
            assertFalse(appointments[1].occursOn(2020, 9, 15));
            assertFalse(appointments[2].occursOn(2020, 9, 15));
        }

        @Test
        void save() throws IOException {



            Appointment[] appointments = new Appointment[3];
            appointments[0] = new Daily("Walk dog", 2020, 10, 15);
            appointments[1] = new Monthly("Pay rent", 2020, 10, 15);
            appointments[2] = new Onetime("Complete essay", 2020, 10, 15);

            Appointment.save("appointments.txt", appointments[0]);
            Appointment.save("appointments.txt", appointments[1]);
            Appointment.save("appointments.txt", appointments[2]);

            File myFile = new File("appointments.txt");
            Scanner inputFile = new Scanner(myFile);

            assertEquals("2020-10-15-Walk dog",inputFile.nextLine());
            assertEquals("2020-10-15-Pay rent",inputFile.nextLine());
            assertEquals("2020-10-15-Complete essay",inputFile.nextLine());

        }

        @Test
        void load() throws IOException {

            ArrayList<Appointment> appointments = new ArrayList<>();

            ArrayList<Appointment> loadedAppointments = new ArrayList<>();

            appointments.add(new Daily("Walk dog", 2020, 10, 15));
            appointments.add(new Monthly("Pay rent", 2020, 10, 15));
            appointments.add(new Onetime("Complete essay", 2020, 10, 15));

            appointments.get(0).save("Daily", appointments.get(0));

            loadedAppointments = appointments.get(0).load("Daily");


            assertSame(appointments.get(0).getDay(), loadedAppointments.get(0).getDay());
            assertSame(appointments.get(0).getMonth(), loadedAppointments.get(0).getMonth());



        }


}