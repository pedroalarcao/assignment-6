import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;


class Appointment {

    protected String type;
    String description;
    int year;
    int month;
    int day;


    Appointment(String description, int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.description = description;
    }

    public boolean occursOn(int year, int month, int day) {


        return true;
    }

    public int getDay() {
        return day;
    }

    public int getMonth() { return month; }

    public int getYear() {
        return year;
    }

    public static void save (String file, Appointment appointment) throws IOException {

        FileWriter fwriter = new FileWriter(file, true);

        PrintWriter outputFile = new PrintWriter(fwriter);

        outputFile.print(appointment.year + "-" + appointment.month + "-" + appointment.day + "-");
        outputFile.println(appointment.description);

        outputFile.close();

    }

    public static ArrayList<Appointment> load (String fileName) throws IOException {

        File myFile = new File(fileName);
        Scanner inputFile = new Scanner(myFile);
        inputFile.useDelimiter("-|\n");


        ArrayList<Appointment> loadedAppointments = new ArrayList<>();
        while(inputFile.hasNext()){
            int year = inputFile.nextInt();
            int month = inputFile.nextInt();
            int day = inputFile.nextInt();
            String description = inputFile.next();
            if(fileName == "Daily"){
                loadedAppointments.add(new Daily(description, year, month, day));
            }
            if(fileName == "Monthly"){
                loadedAppointments.add(new Monthly(description, year, month, day));
            }
            if(fileName == "Onetime"){
                loadedAppointments.add(new Onetime(description, year, month, day));
            }

        }
        FileWriter fwriter = new FileWriter("loadedAppointments.txt", true);

        PrintWriter outputFile = new PrintWriter(fwriter);
        for (int i = 0; i < loadedAppointments.size(); i++) {
            outputFile.print(loadedAppointments.get(i).year + "-" + loadedAppointments.get(i).getMonth() + "-" + loadedAppointments.get(i).getDay() + "-");
            outputFile.print(loadedAppointments.get(i).description);
        }
        outputFile.close();

        return loadedAppointments;
    }
}

class Daily extends Appointment {

    Daily(String description, int year, int month, int day) {
        super(description, year, month, day);
    }


    // Has to be same or later year (Later year auto includes)
    // If same year, has to be same or later month (Later month auto includes)
    // If same month, has to be same or later day
    //returns true if those conditions are met
    public boolean occursOn(int year, int month, int day) {

        if (year > getYear())
            return true;
        if (year == getYear() && month > getMonth())
            return true;
        if (year == getYear() && month == getMonth() && day > getDay())
            return true;
        if (year == getYear() && month == getMonth() && day == getDay())
            return true;

        return false;
    }
}

class Onetime extends Appointment {

    Onetime(String description, int year, int month, int day) {
        super(description, year, month, day);
    }

    // Has to be same year && same month && same day
    //returns true if those conditions are met
    public boolean occursOn(int year, int month, int day) {

        return (getYear() == year) && (getMonth() == month) && (getDay() == day);
    }

}

class Monthly extends Appointment {

    Monthly(String description, int year, int month, int day) {
        super(description, year, month, day);
    }

    // Has to be same or later year but, if same year,  has to be same or later month. Has to be same day.
    //returns true if those conditions are met
    public boolean occursOn(int year, int month, int day) {

        if (year == getYear() && month >= getMonth() && day == getDay())
            return true;
        if (year > getYear() && day == getDay())
            return true;
        else
            return false;
    }
}


public class P9_5 {
    public static void main(String[] args) throws IOException {

        Scanner input = new Scanner(System.in);

        // ArrayList of Appointment objects
        ArrayList<Appointment> appointments = new ArrayList<Appointment>();


        char selection;
        do {
            System.out.println("Select an option: A for add an appointment, C for checking, L to load appointments, Q to quit: ");
            selection = input.next().charAt(0);
            if (selection == 'A') {
                //since it comes after a next(), we need to have a nextLine() so that it doesn't conflict with next input
                input.nextLine();

                System.out.println("Enter the type (O - OneTime, D - Daily, or M - Monthly): ");
                char typeInput = input.next().charAt(0);
                input.nextLine();

                System.out.println("Enter the date (yyyy-mm-dd): ");
                String dateInput = input.next();

                //sets date is at this format
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                //stores date at localDate as a LocalDate object which helps us get() each value of date separately
                LocalDate localDate = LocalDate.parse(dateInput, formatter);
                //since it comes after a next(), we need to have a nextLine() so that it doesn't conflict with next input
                input.nextLine();

                System.out.println("Enter the description: ");
                String descriptionInput = input.nextLine();

                if (typeInput == 'O') {
                    //creates a Onetime object with the param that user inputted and stores it in ArrayList
                    appointments.add(0, new Onetime(descriptionInput, localDate.getYear(), localDate.getMonthValue(), localDate.getDayOfMonth()));

                }
                if (typeInput == 'D') {
                    //creates a Daily object with the param that user inputted and stores it in ArrayList
                    appointments.add(0, new Daily(descriptionInput, localDate.getYear(), localDate.getMonthValue(), localDate.getDayOfMonth()));
                }
                if (typeInput == 'M') {
                    //creates a Monthly object with the param that user inputted and stores it in ArrayList
                    appointments.add(0, new Monthly(descriptionInput, localDate.getYear(), localDate.getMonthValue(), localDate.getDayOfMonth()));
                }
                appointments.get(0).save(appointments.get(0).getClass().getSimpleName(), appointments.get(0));

            }
            if (selection == 'C') {
                System.out.println("Enter year: ");
                int year = input.nextInt();
                //since it comes after a nextInt(), we need to have a nextLine() so that it doesn't conflict with next input
                input.nextLine();
                System.out.println("Enter month: ");
                int month = input.nextInt();
                //since it comes after a nextInt(), we need to have a nextLine() so that it doesn't conflict with next input
                input.nextLine();
                System.out.println("Enter day: ");
                int day = input.nextInt();

                System.out.println("Appointments: ");

                //Traverses the ArrayList of appointments
                for (Appointment appointment : appointments) {
                    //if Appointment object at this index returns true for method occursOn, it will print out description of object
                    if (appointment.occursOn(year, month, day)) {
                        System.out.println(appointment.description);
                    }
                }
            }
            if (selection == 'L'){
                System.out.println("Enter the type (O - OneTime, D - Daily, or M - Monthly): ");
                char typeInput = input.next().charAt(0);
                input.nextLine();

                if (typeInput == 'O') {
                    for (Appointment appointment : appointments) {
                        appointment.load("Onetime");
                    }
                }
                if (typeInput == 'D') {
                    for (Appointment appointment : appointments) {
                        appointment.load("Daily");
                    }
                }
                if (typeInput == 'M') {
                    for (Appointment appointment : appointments) {
                        appointment.load("Monthly");
                    }
                }
            }
            System.out.println(" ");
        }while (selection != 'Q');

    }

}
