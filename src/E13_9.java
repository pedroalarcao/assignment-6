public class E13_9 {

    public static void main(String[] args) {

        System.out.println(indexOf("Mississipi", "sip"));
    }

    public static int indexOf(String text, String str) {

        //case that both the text and the substring is nothing, therefore they match at first index
        if (text.length() == 0 && str.length() == 0) {
            return 0;
        }
        //case that there's no text to be matched with but str has something.
        else if (text.length() == 0) {
            return -1;
        }
        //only in case that both chars at beginning match but text goes on and str ends
        else if (str.length() == 0) {
            return 0;
        }

        //str is bigger therefore it doesn't exist in text
        else if (str.length() > text.length()) {
            return -1;
        }

        //case that they beginning match and they go all the way through matching
        else if (text.charAt(0) == str.charAt(0)) {

            //does not need to update index+1 since it happens at first char and will keep happening if str and text matched further into it, if not -1
            return indexOf(text.substring(1), str.substring(1));

        }
        //past the first chars. substring(1) basically keeps cutting the lenght of text string
        else {
            int index = indexOf(text.substring(1), str);
            // case that on way back it hits one of the -1 conditions which means they don't match from that point
            if(index == -1){
                return -1;
            }
            else{
                return index + 1;
            }
        }

    }
}
